/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2009-2020 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */

package org.openbravo.module.cifnifvalidator.erpCommon.ad_callouts;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.erpCommon.utility.OBMessageUtils;

public class SE_CIFNIF_Validator extends SimpleCallout {

  @Override
  protected void execute(CalloutInfo info) throws ServletException {
    final String strTaxID = info.getStringParameter("inptaxid");
    if (StringUtils.isNotBlank(strTaxID) && !(isValidCIF(strTaxID) || isValidNIF(strTaxID))) {
      info.addResult("INFO", OBMessageUtils.messageBD("CIFNIF_InvalidTaxId"));
    }
  }

  /**
   * Checks whether the string is a valid CIF
   * 
   * @param cif
   * @return
   */
  private boolean isValidCIF(String cif) {

    Pattern cifPattern = Pattern.compile(
        "([ABCDEFGHJNPQRSUVWabcdefghjnpqrsuvw])(\\d)(\\d)(\\d)(\\d)(\\d)(\\d)(\\d)([abcdefghijABCDEFGHIJ0123456789])");

    Matcher m = cifPattern.matcher(cif);
    if (m.matches()) {
      if (log4j.isDebugEnabled()) {
        log4j.debug("CIF match the pattern:");
      }

      // Add positions 3, 5, and 7
      int addEven = Integer.parseInt(m.group(3)) + Integer.parseInt(m.group(5))
          + Integer.parseInt(m.group(7));

      if (log4j.isDebugEnabled()) {
        log4j.debug("Even sum: " + addEven);
      }

      // Multiply by 2 positions 2,4,6,8 and add the digits (/10 and %10)
      int add2 = ((Integer.parseInt(m.group(2)) * 2) % 10)
          + ((Integer.parseInt(m.group(2)) * 2) / 10);
      int add4 = ((Integer.parseInt(m.group(4)) * 2) % 10)
          + ((Integer.parseInt(m.group(4)) * 2) / 10);
      int add6 = ((Integer.parseInt(m.group(6)) * 2) % 10)
          + ((Integer.parseInt(m.group(6)) * 2) / 10);
      int add8 = ((Integer.parseInt(m.group(8)) * 2) % 10)
          + ((Integer.parseInt(m.group(8)) * 2) / 10);

      // Add positions 2,4,6,7
      int addOdd = add2 + add4 + add6 + add8;

      if (log4j.isDebugEnabled()) {
        log4j.debug("Odd sum: " + addOdd);
      }

      int addition = addEven + addOdd;
      int control = 10 - (addition % 10);
      // The string starts with 0, J is 0 (not 10)
      if (control == 10) {
        control = 0;
      }
      String characters = "JABCDEFGHI";

      if (log4j.isDebugEnabled()) {
        log4j.debug("Control: " + control + " or " + characters.substring(control, control + 1));
      }

      // Control Digit is a char
      if (m.group(1).equalsIgnoreCase("N") || m.group(1).equalsIgnoreCase("P")
          || m.group(1).equalsIgnoreCase("Q") || m.group(1).equalsIgnoreCase("R")
          || m.group(1).equalsIgnoreCase("S") || m.group(1).equalsIgnoreCase("W")) {

        if (log4j.isDebugEnabled()) {
          log4j.debug("Have to be a character");
        }
        if (m.group(9).equalsIgnoreCase(characters.substring(control, control + 1))) {
          return true;
        } else {
          return false;
        }
      }
      // Control Digit is an integer
      else if (m.group(1).equalsIgnoreCase("H") || m.group(1).equalsIgnoreCase("J")
          || m.group(1).equalsIgnoreCase("U") || m.group(1).equalsIgnoreCase("V")) {

        if (log4j.isDebugEnabled()) {
          log4j.debug("Have to be a number");
        }
        if (m.group(9).equalsIgnoreCase("" + control)) {
          return true;
        } else {
          return false;
        }
      }
      // Control Digit is an integer or a char
      else {
        if (m.group(9).equalsIgnoreCase(characters.substring(control, control + 1))
            || m.group(9).equalsIgnoreCase("" + control)) {
          return true;
        } else {
          return false;
        }
      }
    } else {
      return false;
    }
  }

  /**
   * Checks whether the string is a valid DNI
   * 
   * @param nif
   * @return
   */
  private boolean isValidNIF(String nif) {
    Pattern nifPattern = Pattern
        .compile("(\\d{1,8})([TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke])");
    Matcher m = nifPattern.matcher(nif);
    if (m.matches()) {
      String[] nifArray = new String[2];
      nifArray[0] = nif.substring(0, nif.length() - 1);
      nifArray[1] = nif.substring(nif.length() - 1, nif.length());
      int character = (Integer.valueOf(nifArray[0]).intValue()) % 23;
      String[] validChars = { "T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J",
          "Z", "S", "Q", "V", "H", "L", "C", "K", "E", "T" };

      if (validChars[character].compareToIgnoreCase(nifArray[1]) == 0) {
        return true;
      }
    }
    return false;
  }

}
